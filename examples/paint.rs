extern crate gl;
extern crate glutin;
extern crate gl_context;
extern crate mat4;
extern crate mat32;
extern crate vec2;

extern crate gl_canvas;


use glutin::GlContext;
use gl::types::*;
use gl_context::{Context, Usage, DrawMode, BufferTarget};

use gl_canvas::{CanvasShared, Canvas};


static VS: &'static str = "
    #version 120

    uniform mat4 projection;
    uniform mat4 model_view;
    uniform vec2 size;

    attribute vec2 position;
    attribute vec2 uv;

    varying vec2 v_uv;

    void main() {
        gl_Position = projection * model_view * vec4(position * (size * 0.5), 0.0, 1.0);
        v_uv = uv;
    }
";
static FS: &'static str = "
    #version 120

    uniform sampler2D diffuse;

    varying vec2 v_uv;

    void main() {
        gl_FragColor = texture2D(diffuse, v_uv);
    }
";


static SQUARE_VERTEX_DATA: [GLfloat; 16] = [
    // vertices           uvs
     1f32,  1f32,   1f32, 1f32,
    -1f32,  1f32,   0f32, 1f32,
     1f32, -1f32,   1f32, 0f32,
    -1f32, -1f32,   0f32, 0f32
];


struct Camera {
    width: usize,
    height: usize,
    view: [f32; 6],
    projection: [f32; 6],
}

impl Camera {

    #[inline]
    fn new(w: usize, h: usize) -> Self {
        let mut camera = Camera {
            width: 1_usize,
            height: 1_usize,
            projection: mat32::new_identity(),
            view: mat32::new_identity(),
        };
        camera.set_size(w, h);
        camera
    }

    #[inline]
    fn screen_to_world<'a, 'b>(&self, out: &'a mut [f32; 2], screen: &'b [f32; 2]) -> &'a mut [f32; 2] {
        out[0] = 2_f32 * (screen[0] / (self.width as f32)) - 1_f32;
        out[1] = -2_f32 * (screen[1] / (self.height as f32)) + 1_f32;

        let mut tmp = mat32::new_identity();
        mat32::mul(&mut tmp, &self.projection, &self.view);

        let tmp_mat = tmp.clone();
        mat32::inverse(&mut tmp, &tmp_mat);

        let tmp_vec = out.clone();
        vec2::transform_mat32(out, &tmp_vec, &tmp);

        out
    }

    #[inline]
    fn update_projection(&mut self) {
        let r = (self.width as f32) / 2_f32;
        let l = -r;
        let t = (self.height as f32) / 2_f32;
        let b = -t;
        mat32::orthographic(&mut self.projection, t, r, b, l);
    }
    #[inline]
    fn set_size(&mut self, w: usize, h: usize) -> &mut Self {
        self.width = w;
        self.height = h;
        self.update_projection();
        self
    }

    #[inline(always)]
    fn view(&self) -> &[f32; 6] { &self.view }
    #[inline(always)]
    fn projection(&self) -> &[f32; 6] { &self.projection }
}


fn main() {
    let mut screen_width = 1024_usize;
    let mut screen_height = 768_usize;

    let mut events_loop = glutin::EventsLoop::new();
    let window = glutin::WindowBuilder::new()
        .with_title("GL Canvas Paint")
        .with_dimensions(screen_width as u32, screen_height as u32);
    let ctx = glutin::ContextBuilder::new()
        .with_vsync(false);
    let gl_window = glutin::GlWindow::new(window, ctx, &events_loop).unwrap();

    let mut context = Context::new();

    unsafe {
        gl_window.make_current().unwrap();
    }

    gl::load_with(|symbol| gl_window.get_proc_address(symbol) as *const _);

    context.init();

    println!("{:?}", context.version());
    println!(
        "OpenGL version: {:?}.{:?}, GLSL version {:?}.{:?}0",
        context.major(), context.minor(), context.glsl_major(), context.glsl_minor()
    );

    let canvas_shared = CanvasShared::new(&mut context);
    let mut canvas = Canvas::new(&mut context, 960, 640);


    let square_vertex_array = context.new_vertex_array();
    context.set_vertex_array(&square_vertex_array, false);

    let mut square_buffer = context.new_buffer();
    square_buffer.set(BufferTarget::Array, &SQUARE_VERTEX_DATA, 4, Usage::StaticDraw);


    let mut program = context.new_program();
    program.set(VS, FS);


    let mut playing = true;

    let mut camera = Camera::new(screen_width, screen_height);

    canvas.bind(&mut context);
    canvas.clear(&mut context, &[1_f32; 4]);
    canvas.unbind(&mut context);


    let mut mouse_position = [0f32; 2];
    let mut mouse_world_position = [0f32; 2];
    let mut mouse = [0f32; 2];
    let mut last_mouse = [0f32; 2];
    let mut mouse_down = false;

    let color = [0.3, 0.3, 0.3, 1.0];
    let mut projection = mat4::new_identity::<f32>();
    let mut model_view = mat4::new_identity::<f32>();

    while playing {

        events_loop.poll_events(|event| {
            match event {
                glutin::Event::WindowEvent { event: glutin::WindowEvent::Closed, .. } => {
                    playing = false;
                },
                glutin::Event::WindowEvent { event: glutin::WindowEvent::Resized(w, h), .. } => {
                    screen_width = w as usize;
                    screen_height = h as usize;

                    camera.set_size(screen_width, screen_height);
                    gl_window.resize(w, h);
                },
                glutin::Event::WindowEvent { event: glutin::WindowEvent::MouseMoved { position, .. }, .. } => {
                    let (x, y) = position;
                    mouse_position[0] = x as f32;
                    mouse_position[1] = y as f32;
                    camera.screen_to_world(&mut mouse_world_position, &mouse_position);
                },
                glutin::Event::WindowEvent { event: glutin::WindowEvent::MouseInput { state, .. }, .. } => {
                    match state {
                        glutin::ElementState::Pressed => { mouse_down = true; },
                        glutin::ElementState::Released => { mouse_down = false; },
                    }
                },
                _ => (),
            }
        });

        last_mouse[0] = mouse[0];
        last_mouse[1] = mouse[1];
        mouse[0] = (mouse_world_position[0] / canvas.width() as f32) + 0.5f32;
        mouse[1] = (mouse_world_position[1] / canvas.height() as f32) + 0.5f32;

        if mouse_down {
            canvas.bind(&mut context);
            canvas.line(
                &mut context,
                &canvas_shared,
                &last_mouse,
                &mouse,
                &[0.0, 0.0, 0.0, 1.0],
                4_f32
            );
            canvas.unbind(&mut context);
        }

        mat4::from_mat32(&mut projection, camera.projection());
        mat4::from_mat32(&mut model_view, camera.view());

        // render to screen

        context.set_viewport(0, 0, screen_width, screen_height);
        context.set_clear_color(&color);
        context.clear(true, true, true);

        context.set_program(&program, false);
        context.set_vertex_array(&square_vertex_array, false);

        program.set_attribute("position", &mut context, &square_buffer, 0, false);
        program.set_attribute("uv", &mut context, &square_buffer, 2, false);

        program.set_uniform("size", &mut context, &[canvas.width() as f32, canvas.height() as f32], false);
        program.set_uniform("diffuse", &mut context, canvas.texture(), false);
        program.set_uniform("projection", &mut context, &projection, false);
        program.set_uniform("model_view", &mut context, &model_view, false);

        context.draw_arrays(DrawMode::TriangleStrip, 0, 4);

        gl_window.swap_buffers().unwrap();
    }
}
