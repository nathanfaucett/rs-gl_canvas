use gl_context::{Context, VertexArray, Buffer, BufferTarget, Usage};


static RECT_VERTEX_DATA: [f32; 16] = [
    // vertices     uvs
     1f32,  1f32,   1f32, 1f32,
    -1f32,  1f32,   0f32, 1f32,
     1f32, -1f32,   1f32, 0f32,
    -1f32, -1f32,   0f32, 0f32
];


pub struct CanvasShared {
    rect_buffer: Buffer,
    rect_vertex_array: VertexArray,
}

impl CanvasShared {

    #[inline]
    pub fn new(context: &mut Context) -> Self {
        let rect_vertex_array = context.new_vertex_array();
        let mut rect_buffer = context.new_buffer();

        context.set_vertex_array(&rect_vertex_array, false);
        rect_buffer.set(BufferTarget::Array, &RECT_VERTEX_DATA, 4, Usage::StaticDraw);

        CanvasShared {
            rect_buffer: rect_buffer,
            rect_vertex_array: rect_vertex_array,
        }
    }

    #[inline(always)]
    pub fn rect_buffer(&self) -> &Buffer { &self.rect_buffer }
    #[inline(always)]
    pub fn rect_buffer_mut(&mut self) -> &mut Buffer { &mut self.rect_buffer }

    #[inline(always)]
    pub fn rect_vertex_array(&self) -> &VertexArray { &self.rect_vertex_array }
    #[inline(always)]
    pub fn rect_vertex_array_mut(&mut self) -> &mut VertexArray { &mut self.rect_vertex_array }
}
