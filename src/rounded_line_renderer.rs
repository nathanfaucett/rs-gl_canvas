use gl_context::{Context, DrawMode, Program};

use super::canvas_shared::CanvasShared;


static ROUNDED_LINE_RENDERER_VS: &'static str = "
    #version 120

    attribute vec2 position;
    attribute vec2 uv;

    varying vec2 v_uv;

    void main() {
        gl_Position = vec4(position, 0.0, 1.0);
        v_uv = uv;
    }
";
static ROUNDED_LINE_RENDERER_FS: &'static str = "
    #version 120

    #define M_PI 3.1415926535897932384626433832795

    uniform vec2 start;
    uniform vec2 end;
    uniform vec4 color;
    uniform float radius;
    uniform float hardness;
    uniform float inv_aspect;

    varying vec2 v_uv;

    bool is_nan(float val) {
        return !(val < 0.0 || 0.0 < val || val == 0.0);
    }

    void line(vec2 p1, vec2 p2, vec2 uv, float radius, out float distLine, out float distp1) {
        if (distance(p1,p2) <= 0.0) {
            distLine = distance(uv, p1);
            distp1 = 0.0;
            return;
        }

        float a = abs(distance(p1, uv));
        float b = abs(distance(p2, uv));
        float c = abs(distance(p1, p2));
        float d = sqrt(c*c + radius*radius);

        vec2 a1 = normalize(uv - p1);
        vec2 b1 = normalize(uv - p2);
        vec2 c1 = normalize(p2 - p1);

        if (dot(a1,c1) < 0.0) {
            distLine = a;
            distp1 = 0.0;
            return;
        }
        if (dot(b1,c1) > 0.0) {
            distLine = b;
            distp1 = 1.0;
            return;
        }

        float p = (a + b + c) * 0.5;
        float h = 2.0 / c * sqrt( p * (p-a) * (p-b) * (p-c) );

        if (is_nan(h)) {
            distLine = 0.0;
            distp1 = a / c;
        } else {
            distLine = h;
            distp1 = sqrt(a*a - h*h) / c;
        }
    }

    void main() {
        float distLine, distp1;
        vec2 adj_start = vec2(start.x, start.y * inv_aspect);
        vec2 adj_end = vec2(end.x, end.y * inv_aspect);

        vec2 aspect_uv = vec2(v_uv.x, v_uv.y * inv_aspect);
        line(adj_start, adj_end, aspect_uv, radius, distLine, distp1);

        float scale = 1.0 / (2.0 * radius * (1.0 - hardness));
        float period = M_PI * scale;
        float phase = (1.0 - scale * 2.0 * radius) * M_PI * 0.5;
        float alpha = cos((period * distLine) + phase);

        if (distLine < radius - (0.5 / scale)) {
            alpha = 1.0;
        } else if (distLine > radius) {
            alpha = 0.0;
        }

        // float final_alpha = max(t.a, alpha * color.a);
        float final_alpha = alpha * color.a;
        gl_FragColor = vec4(color.rgb, clamp(final_alpha, 0.0, 1.0));
    }
";


pub struct RoundedLineRenderer {
    program: Program,
}

impl RoundedLineRenderer {

    #[inline]
    pub fn new(context: &Context) -> Self {
        let mut program = context.new_program();

        program.set(ROUNDED_LINE_RENDERER_VS, ROUNDED_LINE_RENDERER_FS);

        RoundedLineRenderer {
            program: program,
        }
    }

    #[inline(always)]
    pub fn render(
        &mut self,
        context: &mut Context,
        canvas_shared: &CanvasShared,
        start: &[f32; 2],
        end: &[f32; 2],
        color: &[f32; 4],
        radius: f32,
        hardness: f32,
        width: f32,
        height: f32
    ) -> &Self {
        let inv_aspect = height / width;

        context.set_program(&self.program, false);
        context.set_vertex_array(canvas_shared.rect_vertex_array(), false);

        self.program.set_attribute("position", context, canvas_shared.rect_buffer(), 0, false);
        self.program.set_attribute("uv", context, canvas_shared.rect_buffer(), 2, false);

        self.program.set_uniform("start", context, start, false);
        self.program.set_uniform("end", context, end, false);
        self.program.set_uniform("radius", context, &radius, false);
        self.program.set_uniform("hardness", context, &hardness, false);
        self.program.set_uniform("color", context, color, false);
        self.program.set_uniform("inv_aspect", context, &inv_aspect, false);

        context.draw_arrays(DrawMode::TriangleStrip, 0, 4);

        self
    }

    #[inline(always)]
    pub fn program(&self) -> &Program { &self.program }
}
