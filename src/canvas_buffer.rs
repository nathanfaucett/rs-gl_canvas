use gl_context::{
    Context, TextureKind, TextureFormat, TextureWrap, FilterMode,
    Attachment, Texture, Framebuffer, Renderbuffer,
};


pub struct CanvasBuffer {
    width: usize,
    height: usize,
    texture: Texture,
    framebuffer: Framebuffer,
    renderbuffer: Renderbuffer,
}

impl CanvasBuffer {

    #[inline]
    pub fn new(context: &Context, width: usize, height: usize) -> Self {
        let mut texture = context.new_texture();

        texture.init_null_texture_2d(
            &context,
            width,
            height,
            TextureFormat::RGBA,
            TextureKind::UnsignedByte,
            TextureWrap::Clamp,
            FilterMode::None,
            false
        );

        let framebuffer = context.new_framebuffer();
        framebuffer.set(&context, &texture, &[Attachment::Color], 0);

        let renderbuffer = context.new_renderbuffer();
        renderbuffer.set(&context, TextureFormat::DepthComponent, Attachment::Depth, width, height);

        CanvasBuffer {
            width: width,
            height: height,
            texture: texture,
            framebuffer: framebuffer,
            renderbuffer: renderbuffer,
        }
    }

    #[inline]
    pub fn resize(&mut self, context: &Context) -> &Self {

        self.texture.set_null_image_2d(
            self.width,
            self.height,
            TextureFormat::RGBA,
            TextureKind::UnsignedByte
        );
        self.framebuffer.set(context, &self.texture, &[Attachment::Color], 0);
        self.renderbuffer.set(context, TextureFormat::DepthComponent, Attachment::Depth, self.width, self.height);

        self
    }

    #[inline]
    pub fn bind(&self, context: &mut Context) -> &Self {
        context.set_framebuffer(&self.framebuffer, false);
        context.set_renderbuffer(&self.renderbuffer, false);
        context.set_viewport(0, 0, self.width, self.height);
        self
    }
    #[inline]
    pub fn unbind(&self, context: &mut Context) -> &Self {
        context.remove_framebuffer(false);
        context.remove_renderbuffer(false);
        self
    }

    #[inline(always)]
    pub fn width(&self) -> usize { self.width }
    #[inline(always)]
    pub fn height(&self) -> usize { self.height }

    #[inline(always)]
    pub fn texture(&self) -> &Texture { &self.texture }
    #[inline(always)]
    pub fn texture_mut(&mut self) -> &mut Texture { &mut self.texture }

    #[inline(always)]
    pub fn framebuffer(&self) -> &Framebuffer { &self.framebuffer }
    #[inline(always)]
    pub fn framebuffer_mut(&mut self) -> &mut Framebuffer { &mut self.framebuffer }

    #[inline(always)]
    pub fn renderbuffer(&self) -> &Renderbuffer { &self.renderbuffer }
    #[inline(always)]
    pub fn renderbuffer_mut(&mut self) -> &mut Renderbuffer { &mut self.renderbuffer }
}
