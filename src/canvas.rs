use gl_context::{Context, Texture};


use super::canvas_buffer::CanvasBuffer;
use super::rounded_line_renderer::RoundedLineRenderer;
use super::canvas_shared::CanvasShared;


pub struct Canvas {
    canvas_buffer: CanvasBuffer,
    rounded_line_renderer: RoundedLineRenderer,
}

impl Canvas {

    #[inline]
    pub fn new(context: &mut Context, width: usize, height: usize) -> Self {
        Canvas {
            canvas_buffer: CanvasBuffer::new(context, width, height),
            rounded_line_renderer: RoundedLineRenderer::new(context),
        }
    }

    #[inline(always)]
    pub fn bind(&self, context: &mut Context) {
        self.canvas_buffer.bind(context);
    }
    #[inline(always)]
    pub fn unbind(&self, context: &mut Context) {
        self.canvas_buffer.unbind(context);
    }

    #[inline]
    pub fn clear(&self, context: &mut Context, color: &[f32; 4]) {
        context.set_viewport(0, 0, self.canvas_buffer.width(), self.canvas_buffer.height());
        context.set_clear_color(color);
        context.clear(true, true, true);
    }

    #[inline]
    pub fn line(
        &mut self,
        context: &mut Context,
        canvas_shared: &CanvasShared,
        start: &[f32; 2],
        end: &[f32; 2],
        color: &[f32; 4],
        radius: f32
    ) {
        let w = self.canvas_buffer.width() as f32;
        let h = self.canvas_buffer.height() as f32;

        context.clear(false, true, true);

        self.rounded_line_renderer.render(
            context,
            canvas_shared,
            start,
            end,
            color,
            if w < h {radius / w} else {radius / h},
            0.9_f32,
            w, h
        );
    }

    #[inline(always)]
    pub fn texture(&self) -> &Texture { &self.canvas_buffer.texture() }
    #[inline(always)]
    pub fn width(&self) -> usize { self.canvas_buffer.width() }
    #[inline(always)]
    pub fn height(&self) -> usize { self.canvas_buffer.height() }

    #[inline(always)]
    pub fn canvas_buffer(&self) -> &CanvasBuffer { &self.canvas_buffer }
    #[inline(always)]
    pub fn canvas_buffer_mut(&mut self) -> &mut CanvasBuffer { &mut self.canvas_buffer }

    #[inline(always)]
    pub fn rounded_line_renderer(&self) -> &RoundedLineRenderer { &self.rounded_line_renderer }
    #[inline(always)]
    pub fn rounded_line_renderer_mut(&mut self) -> &mut RoundedLineRenderer { &mut self.rounded_line_renderer }
}
